require 'test_helper'

class HostPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get host_pages_home_url
    assert_response :success
  end

end
