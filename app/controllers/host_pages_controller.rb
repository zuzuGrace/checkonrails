class HostPagesController < ApplicationController
  require 'net/http'
  require  'json'

  def home
    url = 'https://api.github.com/repos/rails/rails/issues/state=open' 
    uri = URI(url)
    response = Net::HTTP.get(uri)
    @open_issues = JSON.parse(response)
  end

  def issues
    url = 'https://api.github.com/repos/rails/rails/issues/state=open'   	
  	uri = URI(url)
  	response = Net::HTTP.get(uri)
  	@data = JSON.parse(response)
  end

  def rails_components
    url = 'https://api.github.com/repos/rails/rails/issues/state=open/labels'     
    uri = URI(url)
    response = Net::HTTP.get(uri)
    @labels = JSON.parse(response)
  end

  def comments
    url = 'https://api.github.com/repos/rails/rails/issues/state=open/comments'     
    uri = URI(url)
    response = Net::HTTP.get(uri)
    @comments = JSON.parse(response)
  end

end
