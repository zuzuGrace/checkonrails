# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
		if location.pathname == '/'
			setTimeout ( ->
				location.reload()
				return
			), 50000	

		$('#root-header').mouseover ->
				$(this).css('letter-spacing', '2px')
		$('#root-header').mouseleave ->
				$(this).css('letter-spacing', '1px')

		$('#home-bttn').mouseenter ->
			$(this).css('letter-spacing', '2px')

		$('#home-bttn').mouseleave ->
			$(this).css('letter-spacing', '1px')

		$('#home-bttn').click ->
			$('#issue-count').css("animation-play-state", 'paused')
			$('#issue-highlight').css('opacity', '0.35')
			$('#git-img').css('opacity', '0.35')
			$('.options').fadeIn(250)
	

		$('#git-img').click ->
			$('#issue-count').css("animation-play-state", 'running')
			$('.options').fadeOut(250)
			$('#issue-highlight').css('opacity', '1')
			$('#git-img').css('opacity', '1')

		$('.labels-class').click ->
			
			$('.labels-data').fadeOut(550)
			if @id == 'ac'
				$('#ac-data').fadeIn(550)
			else if @id == 'am'
				$('#am-data').fadeIn(550)
			else if @id == 'ap'
				$('#ap-data').fadeIn(550)
			else if @id == 'av'
				$('#av-data').fadeIn(550)
			else if @id == 'as'
				$('#avs-data').fadeIn(550)
			else if @id == 'avs'
				$('#as-data').fadeIn(550)
			else if @id == 'avr'
				$('#avr-data').fadeIn(550)
			else if @id == 'avm'
				$('#avm-data').fadeIn(550)
			else if @id == 'avp'
				$('#avp-data').fadeIn(550)
			else if @id == 'avj'
				$('#avj-data').fadeIn(550)



		#$('.options').click ->
		#	$('.home-content').fadeOut()
		#	$('.display-issues').fadeOut()
		#	if @id == 'show-rails-issues'
		#		$('#rails-components').fadeIn(250)
		#	else if @id == 'show-git-comments'
		#		$('#comments').fadeIn(250)


		