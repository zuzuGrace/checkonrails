# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 
	5.2.3

* System dependencies
	Pulls Data from the GitHub Rails Api

* Configuration
	The system reloads the home page after 50000 milliseconds. This is done in jQuery as long as the system is on the home page. This is done to update the open-issues parameter from RAILS github API. Once this is accomplished the system is updated and runs on those parameters.

* Database creation
	None - no use of database

* Database initialization
	None - no use of database 

* How to run the test suite
	No test suite to run at this time

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions
	The system can be found https://checkonrails.herokuapp.com/
* ...
