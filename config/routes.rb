Rails.application.routes.draw do
  root 'host_pages#home'

  #query rails repository for the latest, unassigned issues - allow the user to filter the results by component(ActiveRecords etc, Order the results by comment counts, most to least, least to most )
  

  #query rails repo for the latest, unassigned issues
  scope host: 'https://api.github.com/repos/rails/rails/issues'  do 
  	get 'rails_issues' => 'host_pages_#issues', as: :rails_issues
  end

  scope host: 'https://api.github.com/repos/rails/rails/labels' do 
  	get 'rails_labels' => 'host_pages_#rails_components', as: :rails_labels
  end

   scope host: 'https://api.github.com/repos/rails/rails/issues/state=open/comments' do 
  	get 'rails_comments' => 'host_pages_#comments', as: :rails_comments
  end
  #get '/octocat', to: 'https://api.github.com/repos/octocat/Hello-World'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
